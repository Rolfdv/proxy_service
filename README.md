# Proxy service
This project is for the course CS4295 - Release engineering for machine learning applications (REMLA).
This project hosts the proxy service used for REMLA group 8's k8s cluster for Blue-Shadow-Green deployment.

## Package dependencies
This project requires [kubectl](https://kubernetes.io/docs/reference/kubectl/kubectl/) to run. [Minikube](https://minikube.sigs.k8s.io/docs/start/) is an excellent way to get started. Install it via the get started page before continuing.

## Other dependencies
This project requires two other pods to run, which it finds with the environment variables in `my-web.yml`. The project for these pods is found [here](https://gitlab.com/Rolfdv/cs4295_remla_shadow_mode_blue_green).

## Initial start
To first start using this service, run `kubectl apply -f my-web.yml`. Then, start the two pods from [here](https://gitlab.com/Rolfdv/cs4295_remla_shadow_mode_blue_green) with `kubectl apply -f my-models.yml`.

## New version
To deploy a new version, add a version tag to your commit of the format `v<MAJOR>.<MINOR>.<PATCH>`. The CI/CD pipeline will create a new docker container from this commit.
To run this new version with kubectl, run `kubectl rollout restart deploy myweb`.

## What is this project for?
Two environments (pods) can register to this service via the `/register` route, by including their color (either BLUE or GREEN) in a post request.
By default, the pod running the highest version is set to shadow mode when first registered. For example, if BLUE hosts version v1.0.0 and GREEN hosts v1.0.1, GREEN initially runs in shadow mode.
Both pods receive the same traffic, but only the response from the non-shadow mode pod is returned to the requester.

This service provides other endpoints. A post request to `/set_production_model` can be used to manually or automatically update which color pod is the production model.
For example, if BLUE hosts version v1.0.0 and GREEN hosts v1.0.1 and GREEN runs in shadow mode, a post request to `/set_production_model` including `color: GREEN` will target GREEN as production model.
`/metrics` is used to retrieve metrics regarding the pods. Currently this only returns the total amount of requests a pod has received and the total amount of successful responses. In the future, this will be more generalized to fit other needs.

Any request to the default endpoint (`/`) is forwarded to both GREEN and BLUE to predict whatever the request contains. The response from the non-shadow mode pod is returned, the other is ignored.

The goal of this project is to serve as implementation of our proposed Blue-Shadow-Green deployment strategy.