FROM python:3.7.10-slim

WORKDIR /root/
COPY requirements.txt .

RUN python -m pip install --upgrade pip &&\
	pip install -r requirements.txt

COPY pod.py .
COPY proxy_service.py .

EXPOSE 8080

RUN echo "done!"

CMD ["python", "proxy_service.py"]
