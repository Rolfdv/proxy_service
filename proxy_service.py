import os

from flask import Flask, request, jsonify
import requests
import sys
import time

from pod import Pod

START_TIME = time.time()

app = Flask(__name__)
POD_GREEN = os.environ.get("MYMODELS_GREEN_LOCATION")
POD_BLUE = os.environ.get("MYMODELS_BLUE_LOCATION")
model_green: Pod = None
model_blue: Pod = None


@app.route("/", methods=["POST"])
def index():
    if (model_green is None) and (model_blue is None):
        return "No models active", 500

    if model_green is not None:
        resp_green = requests.post(f"{POD_GREEN}/predict", json=request.json)
        model_green.add_request(resp_green.status_code == 200)
        if not model_green.shadow_mode:
            res = resp_green.json()
            res["color"] = model_green.color
    if model_blue is not None:
        resp_blue = requests.post(f"{POD_BLUE}/predict", json=request.json)
        model_blue.add_request(resp_blue.status_code == 200)
        if not model_blue.shadow_mode:
            res = resp_blue.json()
            res["color"] = model_blue.color
    return res


@app.route("/register", methods=["POST"])
def register():
    global model_green, model_blue
    version = request.json.get("version")
    color = request.json.get("color")
    if color == "GREEN":
        model_green = Pod(color, POD_GREEN, version)
    elif color == "BLUE":
        model_blue = Pod(color, POD_BLUE, version)
    else:
        print(f"Unknown color: {color}", file=sys.stderr)
        return "Unknown color", 500

    determine_production_model()

    return "ACK", 200


@app.route("/set_production_model", methods=["POST"])
def set_production_model():
    color = request.json.get("color")
    if color == "GREEN":
        if model_green is not None:
            model_green.shadow_mode = False
            if model_blue is not None:
                model_blue.shadow_mode = True
        else:
            return "Model not yet active", 500
    elif color == "BLUE":
        if model_blue is not None:
            model_blue.shadow_mode = False
            if model_green is not None:
                model_green.shadow_mode = True
        else:
            return "Model not yet active", 500
    else:
        print(f"Unknown color: {color}", file=sys.stderr)
        return "Unknown color", 500

    return "ACK", 200


def determine_production_model():
    global model_green, model_blue
    if model_green is None and model_blue is None:
        return
    elif model_green is None:
        model_blue.shadow_mode = False
    elif model_blue is None:
        model_green.shadow_mode = False
    else:
        model_green.shadow_mode = model_green > model_blue
        model_blue.shadow_mode = not model_green > model_blue
    print(f"GREEN: {model_green}\nBLUE: {model_blue}", file=sys.stderr)


@app.route("/metrics")
def metrics():
    global model_green, model_blue
    res = {"BLUE": None,
           "GREEN": None}
    if model_green is not None:
        res["GREEN"] = model_green.report()
    if model_blue is not None:
        res["BLUE"] = model_blue.report()
    return jsonify(res)


@app.route("/health")
def health():
    return f"{START_TIME}", 200


if __name__ == '__main__':
    print("Starting!")
    app.run("0.0.0.0", 8080, use_reloader=False)
