class Pod:
    def __init__(self, color, location, version, shadow_mode=True):
        self.color = color
        self.location = location
        self.version = version
        self.shadow_mode = shadow_mode

        # Keeps track of how many requests this model has handled
        self.successful_requests = 0
        self.total_requests = 0

    def add_request(self, successful):
        self.total_requests += 1
        if successful:
            self.successful_requests += 1

    def __gt__(self, other):
        assert isinstance(other, Pod)
        if self.version == other.version:
            return False
        versions = [self.version, other.version]
        versions.sort(key=lambda s: [int(u) for u in s.split(".")])
        return versions[1] == self.version

    def __repr__(self):
        return f"{self.__dict__}"

    def report(self):
        return {"successful requests": self.successful_requests,
                "total requests": self.total_requests}